package fr.afpa.unis.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.unis.models.Article;
import fr.afpa.unis.models.Commande;
import fr.afpa.unis.models.Contient;
import fr.afpa.unis.repositories.CommandeRepository;
import fr.afpa.unis.repositories.ContientRepository;

@RestController
public class CommandeController {
    @Autowired
    private CommandeRepository commandeRepository;

    @Autowired
    private ContientRepository contientRepository;
    
    @CrossOrigin
    @GetMapping(value = "/commandes")
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Commande> get() {
        return commandeRepository.findAll();
    }

    @CrossOrigin
    @GetMapping(value = "/commandes/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Optional<Commande> get(@PathVariable(required = true) Integer id) {
        return commandeRepository.findById(id);
    }

    @CrossOrigin
    @PostMapping(value = "/commandes")
    @ResponseStatus(HttpStatus.CREATED)
    public Commande post(@RequestBody Commande commande) {
        // Enregistrer la commande en base de données
        commandeRepository.save(commande);
        // Retourner la commande créée avec son ID auto-généré
        return commande;
    }

    @CrossOrigin
    @PostMapping("/commandes/{id}/articles")
    public ResponseEntity<Contient> addArticleToCommand(
            @PathVariable(value = "id") int commandeId,
            @RequestBody Contient contient) {
        contient.setIdCommande(commandeId);

        // Enregistrer le Contient dans la base de données
        contientRepository.save(contient);

        return ResponseEntity.ok().body(contient);
    }
    
    @CrossOrigin
    @PutMapping("/commandes/{id}")
    public ResponseEntity<Commande> updateCommande(
            @PathVariable(value = "id") int commandeId,
            @RequestBody Commande updatedCommande) {
        // Récupérer la commande existante à partir de la base de données
        Commande existingCommande = commandeRepository.findById(commandeId)
                .orElseThrow();
    
        // Mettre à jour les champs de la commande existante avec les valeurs de la commande mise à jour
        existingCommande.setTraite(true);
    
        // Enregistrer la commande mise à jour dans la base de données
        commandeRepository.save(existingCommande);
    
        return ResponseEntity.ok().body(existingCommande);
    }

    }

