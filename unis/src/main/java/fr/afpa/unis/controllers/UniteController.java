package fr.afpa.unis.controllers;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.unis.models.Article;
import fr.afpa.unis.models.Unite;
import fr.afpa.unis.repositories.UniteRepository;

@RestController
public class UniteController {

    @Autowired
    private UniteRepository uniteRepository;
    
    // @CrossOrigin
    // @GetMapping(value = "/unites/{id}")
    // @ResponseStatus(HttpStatus.OK)
    // public Optional<Unite> get(@PathVariable(required = true) Integer id) {
    //     return uniteRepository.findById(id);
    // }

    @CrossOrigin
    @GetMapping(value = "/unites")
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Unite> getUnites() {
        return uniteRepository.findDistinctUnites();
    }
}
