package fr.afpa.unis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.unis.models.Utilisateur;
import fr.afpa.unis.repositories.UtilisateurRepository;

@RestController
public class UtilisateurController {

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @CrossOrigin
    @PostMapping(value = "/utilisateurs")
    @ResponseStatus(HttpStatus.CREATED)
    public Utilisateur post(@RequestBody Utilisateur utilisateur) {
        // Enregistrer la commande en base de données
        utilisateurRepository.save(utilisateur);
        // Retourner la commande créée avec son ID auto-généré
        return utilisateur;
    }

    // @CrossOrigin
    // @PostMapping("/logins")
    // public ResponseEntity<String> login(@RequestBody Utilisateur loginRequest) {
    //     String login = loginRequest.getLogin();
    //     String motDePasse = loginRequest.getMot_de_passe();

    //     // Vérifier les informations d'identification dans la base de données
    //     Utilisateur utilisateur = utilisateurRepository.findByLogin(login);
    //     if (utilisateur != null && utilisateur.getMot_de_passe().equals(motDePasse)) {
    //         // Les informations d'identification sont valides
    //         return ResponseEntity.ok("Connexion réussie !");
    //     } else {
    //         // Les informations d'identification sont invalides
    //         return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Identifiant ou mot de passe incorrect !");
    //     }
    // }

    @CrossOrigin
    @GetMapping("/logins")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<String> login(@RequestParam("login") String login, @RequestParam("motDePasse") String motDePasse) {
        // Vérifier les informations d'identification dans la base de données
        Utilisateur utilisateur = utilisateurRepository.findByLogin(login);
        if (utilisateur != null && utilisateur.getMotDePasse().equals(motDePasse)) {
            // Les informations d'identification sont valides
            return ResponseEntity.ok("Connexion réussie !");
        } else {
            // Les informations d'identification sont invalides
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Identifiant ou mot de passe incorrect !");
        }
    }
    

}
