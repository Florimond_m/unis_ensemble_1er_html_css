package fr.afpa.unis.controllers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.http.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import fr.afpa.unis.models.Article;
import fr.afpa.unis.models.Commande;
import fr.afpa.unis.models.Contient;
import fr.afpa.unis.models.Unite;
import fr.afpa.unis.repositories.ArticleRepository;
import fr.afpa.unis.repositories.CommandeRepository;
import fr.afpa.unis.repositories.ContientRepository;
import fr.afpa.unis.repositories.UniteRepository;
import fr.afpa.unis.services.FileStorageService;
import jakarta.servlet.http.HttpServletResponse;

@RestController
public class ArticleController {

    @Autowired
    private ArticleRepository articleRepository;
    @Autowired
    private CommandeRepository commandeRepository;
    @Autowired
    private UniteRepository uniteRepository;

    /**
     * Attribut permettant d'utiliser le système de log "slf4j" (API de
     * journalisation Java)
     * Pour plus d'informations sur slf4j ->
     * https://www.baeldung.com/slf4j-with-log4j2-logback
     */
    Logger logger = LoggerFactory.getLogger(FileStorageService.class);

    @Autowired
    private FileStorageService fileStorageService;

    @CrossOrigin
    @GetMapping(value = "/articles")
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Article> get() {
        return articleRepository.findAll();

    }

    @CrossOrigin
    @GetMapping(value = "/articles/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Optional<Article> get(@PathVariable(required = true) Integer id) {
        return articleRepository.findById(id);
    }

    // @CrossOrigin
    // @PostMapping(value = "/articles")
    // @ResponseStatus(HttpStatus.CREATED)
    // public Article post(@RequestBody Article article) {
    // // Rechercher l'unité correspondante dans la base de données
    // Unite unite =
    // uniteRepository.findById(article.getUnite().getId()).orElse(null);
    // article.setUnite(unite);

    // // Enregistrer l'article en base de données
    // articleRepository.save(article);

    // // Retourner l'article créé avec son ID auto-généré
    // return article;
    // }

    @CrossOrigin
    @PostMapping(value = "/articles", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    @ResponseStatus(HttpStatus.OK)
    public Article post(@ModelAttribute Article article) {
        // Unite unite = uniteRepository.findById(article.getUnite().getId()).orElse(null);
        // article.setUnite(unite);
        // articleRepository.save(article);
        try {
            // on récupère l'image provenant de la classe Article (traitement automatique à
            // partir de la requête)
            MultipartFile imageFile = article.getImageFile();
            if (!imageFile.isEmpty()) {
                logger.info("Sauvegarde du fichier image");

                // calcul du hash du fichier pour obtenir un nom unique
                String storageHash = imageFile.getOriginalFilename();
                Path rootLocation = this.fileStorageService.getRootLocation();

                // on retrouve le chemin de stockage de l'image
                Path saveLocation = rootLocation.resolve(storageHash);

                // suppression du fichier au besoin
                Files.deleteIfExists(saveLocation);

                // tentative de sauvegarde
                Files.copy(imageFile.getInputStream(), saveLocation);

                // à ce niveau il n'y a pas eu d'exception
                // on ajoute le nom utilisé pour stocké l'image
                article.setImage(storageHash);
            }
            // on modifie la BDD même sans image
            return articleRepository.save(article);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }

        // Si on arrive là alors erreur
        throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Impossible de sauvegarder la ressource.");
    }

    @GetMapping(value = "/articles/{id}/image", produces = { MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE })
    public @ResponseBody byte[] getImage(@PathVariable int id) {
        Path rootLocation = this.fileStorageService.getRootLocation();
        Optional<Article> article = articleRepository.findById(id);

        if (article.isPresent()) {

            String imageName = article.get().getImage();
            Path imagePath = rootLocation.resolve(imageName);

            try {
                return Files.readAllBytes(imagePath);
            } catch (IOException e) {
                logger.error(e.getMessage());
            }

        }

        try {
            return Files.readAllBytes(rootLocation);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }

        throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Impossible de trouver l'image demandée.");
    }

    /**
     * Retourne l'extension d'un fichier en fonction d'un type MIME
     * pour plus d'informations sur les types MIME :
     * https://developer.mozilla.org/fr/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types
     */
    private String mimeTypeToExtension(String mimeType) {
        return switch (mimeType) {
            case "image/jpeg" -> ".jpeg";
            case "image/png" -> ".png";
            case "image/svg" -> ".svg";
            default -> "";
        };
    }

    // private String generateUniqueFileName(MultipartFile file) {
    // long timestamp = System.currentTimeMillis();
    // return "file_" + timestamp;
    // }
}
