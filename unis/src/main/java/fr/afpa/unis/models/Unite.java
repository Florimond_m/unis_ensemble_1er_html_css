package fr.afpa.unis.models;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="unite")
public class Unite {
        
    @Id //cle primaire
    @GeneratedValue(strategy = GenerationType.IDENTITY) //auto incrementation faire confiance
    private int id;

    @Column(name="nom")
    private String name;

    @Column(name="symbole")
    private String symbole;


    public Unite() {
    }

    public Unite(int id, String name, String symbole) {
        this.id = id;
        this.name = name;
        this.symbole = symbole;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbole() {
        return this.symbole;
    }

    public void setSymbole(String symbole) {
        this.symbole = symbole;
    }

    public Unite id(int id) {
        setId(id);
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", name='" + getName() + "'" +
            ", symbole='" + getSymbole() + "'" +
            "}";
    }
    
}
