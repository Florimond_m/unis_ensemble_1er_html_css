package fr.afpa.unis.models;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonBackReference;

import fr.afpa.unis.models.Contient.ContientId;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

@Entity
@Table(name="contient")
@IdClass(ContientId.class)
public class Contient {
    
    @Id //cle primaire
    @Column(name="id_article")
    private int idArticle;

    @Id //cle primaire
    @Column(name="id_commande")
    private int idCommande;

    @Column(name="quantite")
    private float quantite;

    // @Transient
    @ManyToOne
    @JoinColumn(name="id_article", insertable=false, updatable=false)
    private Article article;

    // @Transient
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name="id_commande", insertable=false, updatable=false)
    private Commande commande;

    // Constructeur, getters et setters

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public Commande getCommande() {
        return commande;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }

    public Contient() {
    }

    public int getIdArticle() {
        return idArticle;
    }

    public void setIdArticle(int idArticle) {
        this.idArticle = idArticle;
    }

    public int getIdCommande() {
        return idCommande;
    }

    public void setIdCommande(int idCommande) {
        this.idCommande = idCommande;
    }

    public float getQuantite() {
        return this.quantite;
    }

    public void setQuantite(float quantite) {
        this.quantite = quantite;
    }


    // Classe imbriquée représentant la clé primaire composite
    
    public static class ContientId implements Serializable {
    
        private int idArticle;

        private int idCommande;

        // Constructeur, getters et setters

        public ContientId() {
        }
    
        public ContientId(int idCommande, int idArticle) {
            this.idCommande = idCommande;
            this.idArticle = idArticle;
        }

        public int getIdCommande() {
            return this.idCommande;
        }
    
        public void setIdCommande(int idCommande) {
            this.idCommande = idCommande;
        }
    
        public int getIdArticle() {
            return this.idArticle;
        }
    
        public void setIdArticle(int idArticle) {
            this.idArticle = idArticle;
        }
    }
}