package fr.afpa.unis.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name="article")
public class Article {
    
    @Id //cle primaire
    @GeneratedValue(strategy = GenerationType.IDENTITY) //auto incrementation faire confiance
    private int id;

    @Column(name="nom")
    private String name;


    @ManyToOne
    @JoinColumn(name="id_unite")
    private Unite unite;

    @Column(name="image")
    private String image;

    @JsonBackReference
    @OneToMany(mappedBy = "article")
    private Set<Contient> contient = new HashSet<>();

    @JsonIgnore
    @Transient
    private MultipartFile imageFile;

    
    public Article() {
    }

    public Article(int id, String name, String image) {
        this.id = id;
        this.name = name;
        this.image = image;

    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public Article id(int id) {
        setId(id);
        return this;
    }


    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", name='" + getName() + "'" +
            "}";
    }

    public Unite getUnite() {
        return unite;
    }

    public void setUnite(Unite unite) {
        this.unite = unite;
    }

    public Set<Contient> getContient() {
        return contient;
    }

    public void setContient(Set<Contient> contient) {
        this.contient = contient;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    public MultipartFile getImageFile() {
        return imageFile;
    }

    public void setImageFile(MultipartFile imageFile) {
        this.imageFile = imageFile;
    }
}
