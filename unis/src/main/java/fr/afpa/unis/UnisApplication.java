package fr.afpa.unis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnisApplication {

	public static void main(String[] args) {
		SpringApplication.run(UnisApplication.class, args);
	}

}
