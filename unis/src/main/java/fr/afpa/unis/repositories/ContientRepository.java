package fr.afpa.unis.repositories;

import org.springframework.data.repository.CrudRepository;

import fr.afpa.unis.models.Contient;

public interface ContientRepository extends CrudRepository<Contient, Integer> {

}