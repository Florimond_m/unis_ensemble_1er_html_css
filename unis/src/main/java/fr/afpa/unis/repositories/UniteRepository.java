package fr.afpa.unis.repositories;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import fr.afpa.unis.models.Article;
import fr.afpa.unis.models.Unite;


public interface UniteRepository extends CrudRepository<Unite, Integer> {
    @Query("SELECT DISTINCT u FROM Unite u")
    Iterable<Unite> findDistinctUnites();
}

