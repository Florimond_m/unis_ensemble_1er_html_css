package fr.afpa.unis.repositories;



import org.springframework.data.repository.CrudRepository;
import fr.afpa.unis.models.Article;


public interface ArticleRepository extends CrudRepository<Article, Integer> {

}
