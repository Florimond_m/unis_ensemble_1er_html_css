package fr.afpa.unis.repositories;


import org.springframework.data.repository.CrudRepository;

import fr.afpa.unis.models.Utilisateur;


public interface UtilisateurRepository extends CrudRepository<Utilisateur, Integer> {
    Utilisateur findByLogin(String login);
}

