package fr.afpa.unis.repositories;

import org.springframework.data.repository.CrudRepository;

import fr.afpa.unis.models.Commande;

public interface CommandeRepository extends CrudRepository<Commande, Integer> {

}