const apiUrl = "http://localhost:8000";


// Sélection de l'élément d'entrée de fichier et de l'élément d'aperçu de l'image
const inputElement = document.getElementById("image-article");
const previewElement = document.getElementById("preview-image");

// Écouteur d'événement pour détecter les changements dans l'entrée de fichier
inputElement.addEventListener("change", function(event) {
    // Vérifier s'il y a des fichiers sélectionnés
    if (event.target.files && event.target.files[0]) {
        // Créer un objet URL pour l'image sélectionnée
        const imageURL = URL.createObjectURL(event.target.files[0]);
        // Mettre à jour l'attribut src de l'élément d'aperçu avec l'URL de l'image
        previewElement.src = imageURL;
    }
});


async function recupUnite() {
    try {
        const response = await fetch(apiUrl + "/unites");
        const data = await response.json();
        const uniteSelect = document.getElementById("unite");

        // Trier les données par ordre alphabétique en fonction des noms des unités
        data.sort((a, b) => a.name.localeCompare(b.name));

        // Créer les options du select à partir des données récupérées
        data.forEach(item => {
            const option = document.createElement("option");
            option.value = item.id;
            option.text = item.name;
            uniteSelect.appendChild(option);
        });
    } catch (error) {
        console.log(error);
    }
}


// async function createArticle(article) {
//     try {
//     let options = {
//         method: 'POST',
//         body: article
//     };

//         const response = await fetch(apiUrl + "/articles", options);
//         const data = await response.json();

//         return data;
//     } catch (error) {
//         console.log("Erreur lors de la création de l'article:", error);
//         throw "Erreur lors de la création de l'article";
//     }
// }




/**
 * 
 * @returns Une promesse englobant le json représentant les stations.
 */
async function getArticles() {
    
    /**
     * Gestion des erreurs avec Try/catch
     */
    try {
        // par défaut le fetch effectue un "GET"
        let res = await fetch(apiUrl);
        return await res.json();
    } catch (error) {
        console.log(error);
    }
}

async function getArticleImage(articleID) {
    try {
        // par défaut le fetch effectue un "GET"
        let res = await fetch(apiUrl + "/" + articleID + "/image");
        return await res.blob();
    } catch (error) {
        console.log(error);
    }
}

// /**
//  * Crée une station en effectuant une requête "POST"
//  * 
//  * @param {*} article Un JSON contenant les informations d'un article 
//  */
async function createArticle(article) {


    // Paramétrage des options pour pouvoir effectuer une méthode "POST"
    // "options" est un JSON !
    let options = {
        method: 'POST',
        body: article,
    };

    try {
        let result = await fetch(apiUrl + "/articles", options);

        // la requête http a pu se faire mais il y a une erreur
        if (!result.ok) {
            throw "Erreur lors de la création de la station";
        }

        return await result.json();
    } catch (error) {
        throw "Erreur lors de la création de l'article";
    }
}



async function validateArticle(event) {
    event.preventDefault();

    const toastElement = document.querySelector("#toast");
    let form = document.querySelector("#create-form");
    let formData = new FormData(form);
    console.log(formData);
    for (let pair of formData.entries())
    {
     console.log(pair[0]+ ', '+ pair[1]); 
    } 

    try {
        await createArticle(formData);

        toastElement.textContent = "Article créé avec succès";
    } catch (error) {
        toastElement.textContent = error;
    }

    toastElement.className = "show";
    setTimeout(function () {
        toastElement.className = toastElement.className.replace("show", "");
    }, 3000);
}

let creerBTN = document.getElementById("creer-nouveau-article");
//document.querySelector('#display-button');
creerBTN.addEventListener('click', validateArticle);

/**
 * Supprime une station en effectuant une requête "DELETE".
 */
async function deleteArticle() {

}




window.addEventListener('DOMContentLoaded', event => {
recupUnite();
});

// export {getArticles, createArticle, deleteArticle, getArticleImage};