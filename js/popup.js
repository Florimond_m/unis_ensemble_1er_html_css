const apiUrl = "http://localhost:8000";

document.getElementById("connexion-form").addEventListener("submit", function(event) {
  event.preventDefault(); // Empêche le formulaire de se soumettre

  // Récupérer les valeurs du formulaire
  var login = document.getElementById("connexion-login").value;
  var motDePasse = document.getElementById("connexion-mot_de_passe").value;

  console.log(login);
  console.log(motDePasse);

  // Envoyer une requête HTTP GET à l'API avec les paramètres d'identification
  var url = apiUrl + "/logins?login=" + encodeURIComponent(login) + "&motDePasse=" + encodeURIComponent(motDePasse);

  fetch(url)
    .then(function(response) {
      response.text().then(function(message) {
        if (response.ok) {
          // Les informations d'identification sont valides
          alert(message);
          handleLoginSuccess();
          // Réinitialiser le formulaire
          document.getElementById("connexion-form").reset();
          // Fermer la pop-up si nécessaire
          closePopup();
        } else {
          // Les informations d'identification sont invalides
          alert(message);
        }
      });
    })
    .catch(function(error) {
      console.error("Une erreur s'est produite lors de la vérification de l'authentification :", error);
      alert("Une erreur s'est produite lors de la vérification de l'authentification. Veuillez réessayer plus tard.");
    });
});


document.getElementById('inscription-form').addEventListener('submit', function(event) {
  event.preventDefault();

  // Récupérer les valeurs des champs du formulaire
  const nom = document.getElementById('nom').value;
  const prenom = document.getElementById('prenom').value;
  const mail = document.getElementById('mail').value;
  const telephone = document.getElementById('telephone').value;
  const login = document.getElementById('inscription-login').value;
  const motDePasse = document.getElementById('inscription-mot_de_passe').value;


  // Créer un objet contenant les données de l'utilisateur
  const utilisateurData = {
    nom: nom,
    prenom: prenom,
    mail: mail,
    telephone: telephone,
    login: login,
    motDePasse: motDePasse,
  };

  // Envoyer les données de l'utilisateur au serveur (ex. via une requête AJAX)

  // Exemple d'utilisation de fetch pour envoyer les données au serveur
  fetch(apiUrl + "/utilisateurs", {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(utilisateurData)
  })
    .then(response => {
      if (response.ok) {
        // L'inscription a réussi
        alert('Inscription réussie !');
        // Réinitialiser les champs du formulaire
        document.getElementById('inscription-form').reset();
        resetPopup();
      } else {
        // L'inscription a échoué
        throw new Error('Erreur lors de l\'inscription.');
      }
    })
    .catch(error => {
      // Gérer les erreurs d'inscription
      console.error(error);
      alert('Erreur lors de l\'inscription.');
    });
});

function resetPopup() {
  document.getElementById("connexion-form").reset();
  document.getElementById("inscription-form").reset();
  document.getElementById("connexion-form").style.display = "block";
  document.getElementById("inscription-form").style.display = "none";
  document.getElementById("inscription-button").style.display = "block";
}

function closePopup() {
  document.getElementById("popup").style.display = "none";
  document.getElementById("inscription-form").style.display = "none"; // Ajout de cette ligne pour masquer le formulaire d'inscription
  document.getElementById("connexion-form").style.display = "block"; // Ajout de cette ligne pour afficher le formulaire de connexion
  resetPopup();
}

function showInscriptionForm() {
  document.getElementById("connexion-form").style.display = "none";
  document.getElementById("inscription-form").style.display = "block";
  document.getElementById("inscription-button").style.display = "none";
}

function openPopup() {
  document.getElementById("popup").style.display = "block";
}

document.addEventListener("DOMContentLoaded", function() {
  // Ajouter l'écouteur d'événement pour le bouton "Connexion Inscription"
  document.getElementById("popup-button").addEventListener("click", function() {
    openPopup();
  });
});

function handleLoginSuccess() {
  // Masquer le bouton de connexion
  var button = document.querySelector('.popup-button');
  button.style.display = 'none';

  // Afficher le nom de l'utilisateur connecté
  var username = getUserUsername(); // Supposons que vous ayez une fonction pour obtenir le nom d'utilisateur
  var nameElement = document.createElement('span');
  nameElement.textContent = username;
  button.parentNode.insertBefore(nameElement, button);
}
