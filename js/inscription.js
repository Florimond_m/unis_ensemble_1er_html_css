const apiUrl = "http://localhost:8000"


document.getElementById('inscription-form').addEventListener('submit', function(event) {
    event.preventDefault();

    // Récupérer les valeurs des champs du formulaire
    const nom = document.getElementById('nom').value;
    const prenom = document.getElementById('prenom').value;
    const mail = document.getElementById('mail').value;
    const telephone = document.getElementById('telephone').value;
    const login = document.getElementById('login').value;
    const motDePasse = document.getElementById('mot_de_passe').value;
   

    // Créer un objet contenant les données de l'utilisateur
    const utilisateurData = {
        nom: nom,
        prenom: prenom,
        mail: mail,
        telephone: telephone,
        login: login,
        mot_de_passe: motDePasse,
    };

    // Envoyer les données de l'utilisateur au serveur (ex. via une requête AJAX)

    // Exemple d'utilisation de fetch pour envoyer les données au serveur
    fetch(apiUrl + "/utilisateurs", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(utilisateurData)
    })
    .then(response => {
        if (response.ok) {
            // L'inscription a réussi
            alert('Inscription réussie !');
            // Réinitialiser les champs du formulaire
            document.getElementById('inscription-form').reset();
        } else {
            // L'inscription a échoué
            throw new Error('Erreur lors de l\'inscription.');
        }
    })
    .catch(error => {
        // Gérer les erreurs d'inscription
        console.error(error);
        alert('Erreur lors de l\'inscription.');
    });
});