const apiUrl = "http://localhost:8000"


const btnGestion = document.getElementById('btn-gestion');
btnGestion.addEventListener('click', function() {
  // Rediriger vers la page gestion-traite.html
  window.location.href = 'gestion.html';
});

function getCommandes() {
    return fetch(apiUrl + "/commandes")
        .then(response => {
            if (!response.ok) {
                throw new Error('Erreur lors de la récupération des commandes.');
            }
            return response.json();
        })
        .then(data => {
            return data;
        })
        .catch(error => {
            console.error('Erreur lors de la récupération des commandes :', error);
            return [];
        });
}


async function afficherCommandeTraite() {
    try {
        const commandes = await getCommandes();
        const listCommandes = document.getElementById('list-commandes-traitees');

        // Filtrer les commandes avec traite = true
        const commandesTraitees = commandes.filter(commande => commande.traite);
        commandesTraitees.forEach(commande => {
            const card = document.createElement('div');
            card.classList.add('card');

            const nom = commande.nom;
            const prenom = commande.prenom;
            const tel = commande.tel;
            const commandeTraite = commande.traite;

            const contient = commande.contient.map(element => {
                const idArticle = element.idArticle;
                const quantite = element.quantite;
                const articleName = element.article.name;
                const articleUnite = element.article.unite.name;
                

                return {
                    idArticle,
                    quantite,
                    articleName,
                    articleUnite
                };
            });      
            // Utilisez les informations récupérées pour créer une card
            // par exemple, ajouter du contenu HTML à la card
            const cardContent = `
          <h2>${nom} ${prenom}</h2>
          <h3>${tel}</h3>
          <h3>${"Traitée"}</h3>
          <ul>
            ${contient.map(item => `<li>${item.articleName} - ${item.quantite} - ${item.articleUnite} </li>`).join('')}
          </ul>
        `;
            card.innerHTML = cardContent;
            listCommandes.appendChild(card);
        });

    } catch (error) {
        // Gérer les erreurs
        const toastElement = document.querySelector("#toast");
        toastElement.textContent = error;
        toastElement.className = "show";
        setTimeout(function () {
            toastElement.className = toastElement.className.replace("show", "");
        }, 3000);
    }
}

afficherCommandeTraite();