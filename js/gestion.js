const apiUrl = "http://localhost:8000"
//gestion des commandes 

const containerCommande = document.getElementById("list-commandes");

const btnGestionTraite = document.getElementById('btn-gestion-traite');
btnGestionTraite.addEventListener('click', function() {
  // Rediriger vers la page gestion-traite.html
  window.location.href = 'gestion-traite.html';
});

function getCommandes() {
    return fetch(apiUrl + "/commandes")
        .then(response => {
            if (!response.ok) {
                throw new Error('Erreur lors de la récupération des commandes.');
            }
            return response.json();
        })
        .then(data => {
            return data;
        })
        .catch(error => {
            console.error('Erreur lors de la récupération des commandes :', error);
            return [];
        });
}



// Appeler la fonction pour créer les cards

async function createCards() {
    try {
        const commandes = await getCommandes();
        const listCommandes = document.getElementById('list-commandes');

        // Filtrer les commandes avec traite = false
    const commandesNonTraitees = commandes.filter(commande => !commande.traite);

        commandesNonTraitees.forEach(commande => {
            const card = document.createElement('div');
            card.classList.add('card');

            const nom = commande.nom;
            const prenom = commande.prenom;
            const tel = commande.tel;

            const contient = commande.contient.map(element => {
                const idArticle = element.idArticle;
                const quantite = element.quantite;
                const articleName = element.article.name;
                const articleUnite = element.article.unite.name;

                return {
                    idArticle,
                    quantite,
                    articleName,
                    articleUnite
                };
            });
            const img2 = document.createElement("img");
            img2.src = "img/green_cross.png"
            img2.classList.add("delete-button");
            img2.addEventListener("click", async function () {
                try {
                    // Mettre à jour la commande côté serveur
                    await updateCommande(commande.id);
                
                    // Supprimer la carte de la page
                    //card.remove();
                  } catch (error) {
                    console.log(error);
                    // Gérer les erreurs
                  }
                });
            
            
            // Utilisez les informations récupérées pour créer une card
            // par exemple, ajouter du contenu HTML à la card
            const cardContent = `
          <h2>${nom} ${prenom}</h2>
          <h3>${tel}</h3>
          <ul>
            ${contient.map(item => `<li>${item.articleName} - ${item.quantite} - ${item.articleUnite} </li>`).join('')}
          </ul>
        `;
            card.innerHTML = cardContent;
            card.appendChild(img2);
            listCommandes.appendChild(card);
        });

    } catch (error) {
        // Gérer les erreurs
        const toastElement = document.querySelector("#toast");
        toastElement.textContent = error;
        toastElement.className = "show";
        setTimeout(function () {
            toastElement.className = toastElement.className.replace("show", "");
        }, 3000);
    }
}

createCards();


async function updateCommande(commandeId) {
    const updateData = {
      traite: true
    };
  
    try {
      const response = await fetch(apiUrl + "/commandes/" + commandeId, {
        method: 'PUT', // ou 'PATCH' selon votre API
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(updateData)
      });
  
      if (!response.ok) {
        throw new Error("Erreur lors de la mise à jour de la commande.");
      }
  
      // La commande a été mise à jour avec succès
    } catch (error) {
      console.log(error);
      throw new Error("Erreur lors de la mise à jour de la commande.");
    }
  }

