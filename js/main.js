

function updateCard(articleType, quantity) {
    const quantityP = document.getElementById("card-" + articleType + "-quantite");
    const unite = uniteArticle.value;
    quantityP.textContent = "Quantité " + quantity + " " + unite;
}



function createCard(container, articleType, articleName, articleQuantity) {



    const img2 = document.createElement("img");
    img2.src = "img/red_cross_sansfond.png"
    img2.classList.add("delete-button");
    img2.addEventListener("click", function () {
        // Supprimer la card
        card.remove();
    });

    let card = document.getElementById("card-" + articleType);

    if (card === null) {

        card = document.createElement("div");

        card.id = "card-" + articleType;
        card.style.display = "flex";

        let title = document.createElement("h3");
        title.innerText = articleName;

        let img = document.createElement("img");
        img.src = apiUrl + "/articles/" + articleType + "/image";
        img.alt = articleName;
        img.style.width = "50%";
        img.style.margin = "auto";


        let quantityP = document.createElement("p");
        quantityP.id = "card-" + articleType + "-quantite";
        card.appendChild(title);
        card.appendChild(img);
        card.appendChild(quantityP);
        card.appendChild(img2);
        container.appendChild(card);
        console.log(articleQuantity);


        img2.style.width = "25%";
        img2.style.margin = "auto";
    }

    updateCard(articleType, articleQuantity);

    // Retourner le conteneur flexbox créé
    return card;
}

// Trouver l'élément dans lequel vous souhaitez ajouter la boîte flexible
const container = document.getElementById("list-course");


let temporaryShoppingList = [];

function addArticle() {
    const article = document.getElementById("article");
    const quantity = document.getElementById("quantite").value;
    if (quantity != null & quantity > 0) {
        temporaryShoppingList.push({
            "idArticle": article.value,
            "quantite": quantity,
            "name": article.options[article.selectedIndex].textContent
        });
        createCard(container, article.value, article.options[article.selectedIndex].textContent, quantity);
    }
}



let articles;

async function recupArticle() {
    try {
        const response = await fetch("http://localhost:8000/articles");
        const data = await response.json();
        const articleSelect = document.getElementById("article");

        // Trier les données par ordre alphabétique en fonction des noms d'articles
        data.sort((a, b) => a.name.localeCompare(b.name));

        // Créer les options du select à partir des données récupérées
        data.forEach(article => {
            const option = document.createElement("option");
            option.value = article.id; // Utiliser l'ID de l'article comme valeur de l'option
            option.text = article.name; // Utiliser le titre de l'article comme texte de l'option
            articleSelect.appendChild(option); // Ajouter l'option au select
        });

        articles = data;
        articleSelect.addEventListener("change", afficherUniteArticle);
    } catch (error) {
        console.log(error);
    }
}

const uniteArticle = document.getElementById("unite");

function afficherUniteArticle() {
    try {
        const selectArticle = document.getElementById("article");
        const idArticle = selectArticle.value;
        console.log(idArticle);
        let articleFind = articles.find(article => {
            if (idArticle == article.id) {
                return true;
            } else {
                return false;
            }
        });
        console.log(articleFind);
        uniteArticle.value = articleFind.unite.symbole;
    } catch (error) {
        console.log(error);
    }
}

recupArticle();

async function addArticleToCommand(commandId, jsonData) {


    // Utilisez fetch() pour envoyer la requête POST à votre API qui insère la nouvelle ligne
    let response = await fetch(apiUrl + "/commandes/" + commandId + "/articles", {
        method: 'POST',
        headers: {
            'Accept': 'application/json; charset=UTF-8',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(jsonData)
    });
    if (!commandId) {
        throw new Error("L'ID de la commande est manquant.");
    }

    if (!response.ok) {
        throw new Error("Erreur lors de l'ajout de l'article à la commande.");
    }

    // Récupérez la réponse JSON qui contient l'ID de la nouvelle ligne créée
    let responseData = await response.json();

    return responseData.id;
}

async function createCommand(command) {

    console.log(JSON.stringify(command));
    // Paramétrage des options pour pouvoir effectuer une méthode "POST"
    // "options" est un JSON !
    let options = {
        method: 'POST',
        headers: {
            'Accept': 'application/json; charset=UTF-8',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(command)
    };

    try {
        let result = await fetch(apiUrl + "/commandes", options);

        return await result.json();
    } catch (error) {
        console.log("On passe par là");
        throw "Erreur lors de la création de la commande";
    }
}


async function validateCommand(event) {
    // par défaut le bouton essaie de rédir
    event.preventDefault();

    // récupération du toast pour affichage d'un message à l'utilisateur
    let toastElement = document.querySelector("#toast");

    try {
        // Création de la commande principale
        let jsonData = {
            "nom": document.querySelector("#name").value,
            "prenom": document.querySelector("#firstname").value,
            "tel": document.querySelector("#phone-number").value,
            // Ajoutez d'autres champs si nécessaire
        };
        console.log(jsonData);
        let newCommand = await createCommand(jsonData);

        // Parcourir la liste de courses temporaire et ajouter chaque article à la commande
        for (let i = 0; i < temporaryShoppingList.length; i++) {

            let articleData = {
                "idArticle": temporaryShoppingList[i].idArticle,
                "quantite": temporaryShoppingList[i].quantite,
            };
            // Ajouter l'article à la commande
            await addArticleToCommand(newCommand.id, articleData);
        }

        // Afficher un message de réussite
        toastElement.textContent = "Commande créée avec succès";
    } catch (error) {
        toastElement.textContent = error;
    }

    // on affiche le toast
    toastElement.className = "show";
    // on cache le toast après 3 secondes
    setTimeout(function () { toastElement.className = toastElement.className.replace("show", ""); }, 3000);
}

const form = document.getElementById('create-form');
document.getElementById('valider-commande').addEventListener('click', validateCommand);


